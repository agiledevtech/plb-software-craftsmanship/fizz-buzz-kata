package plb;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {

    @Test
    void fizzBuzz_echo_should_return_1_when_given_1() {
        // given + when
        String result = new FizzBuzz().echo(1);
        // then
        assertEquals("1", result);
    }

    @Test
    void fizzBuzz_echo_should_return_FizzBuzz_when_given_15() {
        // given + when
        String result = new FizzBuzz().echo(15);
        // then
        assertEquals("FizzBuzz", result);
    }

    @Test
    void fizzBuzz_echo_should_return_Fizz_when_given_9() {
        // given + when
        String result = new FizzBuzz().echo(9);
        // then
        assertEquals("Fizz", result);
    }

    @Test
    void fizzBuzz_echo_should_return_Fizz_when_given_65() {
        // given + when
        String result = new FizzBuzz().echo(65);
        // then
        assertEquals("Buzz", result);
    }
}